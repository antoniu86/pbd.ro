<?php

$title = 'Prima pagina';
$page = 'home';

require_once 'extra/connection.php';
require_once 'extra/head.php';
require_once 'extra/meniu.php';

$result = $conn->query("select id_jucator, nume, invingator, count(invingator) as frecventa from jocuri inner join jucatori on invingator = id_jucator where tip_joc = 'sah' and invingator is not null group by invingator order by frecventa desc limit 1");
$invingator_sah = $result->fetch();

$result = $conn->query("select id_jucator, nume, ((select count(*) from jocuri where jucator1 = id_jucator) + (select count(*) from jocuri where jucator2 = id_jucator)) as sum from jucatori order by sum desc limit 1");
$jucator_activ = $result->fetch();

//var_dump($invingator_sah);

$result = $conn->query("(select id_jucator, nume, invingator, count(invingator) as frecventa, '< 10' as categorie from jocuri inner join jucatori on invingator = id_jucator where invingator is not null and TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) < 10 group by invingator order by frecventa desc limit 1)
UNION
(select id_jucator, nume, invingator, count(invingator) as frecventa, '10 - 18' as categorie from jocuri inner join jucatori on invingator = id_jucator where invingator is not null and (TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) >= 10 and TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) < 18) group by invingator order by frecventa desc limit 1)
UNION
(select id_jucator, nume, invingator, count(invingator) as frecventa, '18 - 40' as categorie from jocuri inner join jucatori on invingator = id_jucator where invingator is not null and (TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) >= 18 and TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) < 40) group by invingator order by frecventa desc limit 1)
UNION
(select id_jucator, nume, invingator, count(invingator) as frecventa, '40 - 50' as categorie from jocuri inner join jucatori on invingator = id_jucator where invingator is not null and (TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) >= 40 and TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) <= 50) group by invingator order by frecventa desc limit 1)
UNION
(select id_jucator, nume, invingator, count(invingator) as frecventa, '> 50' as categorie from jocuri inner join jucatori on invingator = id_jucator where invingator is not null and TIMESTAMPDIFF(YEAR, data_nasterii, CURRENT_DATE) > 50 group by invingator order by frecventa desc limit 1)");
$jucatori = $result->fetchAll();

//var_dump($jucatori);

?>

<section>
  <h1><?php echo $title; ?></h1>
  <p>Cel mai bun jucator de sah este: <a href="jucator.php?action=detalii&id=<?php echo $invingator_sah["id_jucator"]; ?>" class="inpage"><?php echo $invingator_sah["nume"]; ?></a></p>

  <p>Cel mai activ jucator este: <a href="jucator.php?action=detalii&id=<?php echo $jucator_activ["id_jucator"]; ?>" class="inpage"><?php echo $jucator_activ["nume"]; ?></a></p>

  <br><br>

  <p>Sa se afiseze cel mai bun jucator pe categori de varsta (<10, 10-18, 18-40, 40-50, >50). Apar doar daca exista rezultate (1 sql query):</p>

  <?php foreach ($jucatori as $key => $value) { ?>
    <p>Cel mai bun jucator <?php echo $value["categorie"]; ?> este: <a href="jucator.php?action=detalii&id=<?php echo $value["id_jucator"]; ?>" class="inpage"><?php echo $value["nume"]; ?></a></p>
  <?php } ?>
</section>

<?php require_once 'extra/footer.php' ?>
