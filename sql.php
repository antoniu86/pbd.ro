<?php

$title = 'Comenzi SQL';
$page = 'sql';

require_once 'extra/head.php';
require_once 'extra/meniu.php';

?>

<section>
  <h1><?php echo $title; ?></h1>
  <p>b) Să se scrie comenzile SQL pentru tabelele proiectate la punctul anterior.</p>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TABLE</font>&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">EXISTS</font>&nbsp;<font color = "maroon">jucatori</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">id_jucator</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">11</font><font color = "maroon">)</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">NULL</font>&nbsp;<font color = "blue">PRIMARY</font>&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">auto_increment</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">nume</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>VARCHAR</i></font><font color = "maroon">(</font><font color = "black">30</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_nasterii</font>&nbsp;&nbsp;&nbsp;<font color = "black"><i>DATE</i></font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_inscrierii</font>&nbsp;<font color = "black"><i>DATE</i></font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">)</font><font color = "silver">;</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TABLE</font>&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">EXISTS</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">id_joc</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">10</font><font color = "maroon">)</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">NULL</font>&nbsp;<font color = "blue">PRIMARY</font>&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">auto_increment</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">tip_joc</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>VARCHAR</i></font><font color = "maroon">(</font><font color = "black">20</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">jucator1</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">11</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">jucator2</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">11</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">nr_partide</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "black">0</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">nr_partide_jucate</font>&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "black">0</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_inceput_joc</font>&nbsp;&nbsp;<font color = "black"><i>DATETIME</i></font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_sfarsit_joc</font>&nbsp;&nbsp;<font color = "black"><i>DATETIME</i></font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">scor_jucator1</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "black">0</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">scor_jucator2</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "black">0</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">invingator</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">11</font><font color = "maroon">)</font>&nbsp;<font color = "blue">DEFAULT</font>&nbsp;<font color = "blue">NULL</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">fk_jucator1</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">jucator1</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">fk_jucator2</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">jucator2</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">fk_invingator</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">invingator</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">CONSTRAINT</font>&nbsp;<font color = "maroon">fk_jucator1</font>&nbsp;<font color = "blue">FOREIGN</font>&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">jucator1</font><font color = "maroon">)</font>&nbsp;<font color = "blue">REFERENCES</font>&nbsp;<font color = "maroon">jucatori</font>&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">id_jucator</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">CONSTRAINT</font>&nbsp;<font color = "maroon">fk_jucator2</font>&nbsp;<font color = "blue">FOREIGN</font>&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">jucator2</font><font color = "maroon">)</font>&nbsp;<font color = "blue">REFERENCES</font>&nbsp;<font color = "maroon">jucatori</font>&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">id_jucator</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">CONSTRAINT</font>&nbsp;<font color = "maroon">fk_invingator</font>&nbsp;<font color = "blue">FOREIGN</font>&nbsp;<font color = "blue">KEY</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">invingator</font><font color = "maroon">)</font>&nbsp;<font color = "blue">REFERENCES</font>&nbsp;<font color = "maroon">jucatori</font>&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">id_jucator</font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">)</font><font color = "silver">;</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jucatori_t1_insert</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">INSERT</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jucatori</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">CALL</font>&nbsp;<font color = "maroon">chk_date_jucator</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_nasterii</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_inscrierii</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jucatori_t1_update</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">UPDATE</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jucatori</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">CALL</font>&nbsp;<font color = "maroon">chk_date_jucator</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_nasterii</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_inscrierii</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">PROCEDURE</font>&nbsp;<font color = "maroon">chk_date_jucator</font><font color = "maroon">(</font><font color = "blue">IN</font>&nbsp;<font color = "maroon">data_nasterii</font>&nbsp;&nbsp;&nbsp;<font color = "black"><i>DATE</i></font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IN</font>&nbsp;<font color = "maroon">data_inscrierii</font>&nbsp;<font color = "black"><i>DATE</i></font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;<font color = "blue">BEGIN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font>&nbsp;<font color = "maroon">data_nasterii</font>&nbsp;<font color = "silver">&gt;=</font>&nbsp;<font color = "#FF0080"><b>Date_format</b></font><font color = "maroon">(</font><font color = "blue">CURRENT_DATE</font><font color = "silver">,</font>&nbsp;<font color = "maroon">&quot;%y-%m-%d&quot;</font><font color = "maroon">)</font>&nbsp;<font color = "maroon">)</font>&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SIGNAL</font>&nbsp;<font color = "maroon">sqlstate</font>&nbsp;<font color = "red">'90001'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "red">'Data&nbsp;nasterii&nbsp;trebuie&nbsp;sa&nbsp;fie&nbsp;mai&nbsp;mare&nbsp;decat&nbsp;data&nbsp;curenta'</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">ELSEIF</font>&nbsp;<font color = "maroon">(</font>&nbsp;<font color = "maroon">data_inscrierii</font>&nbsp;<font color = "silver">&gt;=</font>&nbsp;<font color = "#FF0080"><b>Date_format</b></font><font color = "maroon">(</font><font color = "blue">CURRENT_DATE</font><font color = "silver">,</font>&nbsp;<font color = "maroon">&quot;%Y-%m-%d&quot;</font><font color = "maroon">)</font>&nbsp;<font color = "maroon">)</font>&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SIGNAL</font>&nbsp;<font color = "maroon">sqlstate</font>&nbsp;<font color = "red">'90002'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "red">'Data&nbsp;inscrierii&nbsp;trebuie&nbsp;sa&nbsp;fie&nbsp;mai&nbsp;mare&nbsp;decat&nbsp;data&nbsp;curenta'</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">END</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "blue">END</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t1</font>
  <br/>&nbsp;&nbsp;<font color = "blue">BEFORE</font>
  <br/>&nbsp;&nbsp;<font color = "blue">INSERT</font>
  <br/>&nbsp;&nbsp;<font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>&nbsp;<font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>&nbsp;<font color = "maroon">begin</font>&nbsp;<font color = "blue">IF</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">id_joc</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "black">1000000000</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">THEN</font>&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90003'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'iID_JOC&nbsp;nu&nbsp;poate&nbsp;fi&nbsp;mai&nbsp;mare&nbsp;de&nbsp;1.000.000.000'</font><font color = "silver">;</font>
  <br/>
  <br/><font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t2_insert</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">INSERT</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">CALL</font>&nbsp;<font color = "maroon">chk_nr_partide</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t2_update</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">UPDATE</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">CALL</font>&nbsp;<font color = "maroon">chk_nr_partide</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">PROCEDURE</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">chk_nr_partide</font><font color = "maroon">(</font><font color = "blue">IN</font>&nbsp;<font color = "maroon">partide</font>&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IN</font>&nbsp;<font color = "maroon">jucate</font>&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font><font color = "maroon">)</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">partide</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "black">100</font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90004'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'numarul&nbsp;de&nbsp;partide&nbsp;nu&nbsp;poate&nbsp;fi&nbsp;mai&nbsp;mare&nbsp;de&nbsp;100'</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "blue">ELSEIF</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font><font color = "maroon">jucate</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "black">100</font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90005'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'numarul&nbsp;de&nbsp;partide&nbsp;jucate&nbsp;nu&nbsp;poate&nbsp;fi&nbsp;mai&nbsp;mare&nbsp;de&nbsp;100'</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t3_insert</font>
  <br/>&nbsp;&nbsp;<font color = "blue">BEFORE</font>
  <br/>&nbsp;&nbsp;<font color = "blue">INSERT</font>
  <br/>&nbsp;&nbsp;<font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>&nbsp;<font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>&nbsp;<font color = "maroon">begin</font>&nbsp;<font color = "blue">IF</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_inceput_joc</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">THEN</font>&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90006'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'data&nbsp;sfarsit&nbsp;joc&nbsp;nu&nbsp;poate&nbsp;fi&nbsp;mai&nbsp;mica&nbsp;decat&nbsp;data&nbsp;inceput&nbsp;joc'</font><font color = "silver">;</font>
  <br/>
  <br/><font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t3_update</font>
  <br/>&nbsp;&nbsp;<font color = "blue">BEFORE</font>
  <br/>&nbsp;&nbsp;<font color = "blue">UPDATE</font>
  <br/>&nbsp;&nbsp;<font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>&nbsp;<font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>&nbsp;<font color = "maroon">begin</font>&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_inceput_joc</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font><font color = "maroon">)</font>&nbsp;<font color = "blue">THEN</font>&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90006'</font>
  <br/>&nbsp;&nbsp;<font color = "blue">SET</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'data&nbsp;sfarsit&nbsp;joc&nbsp;nu&nbsp;poate&nbsp;fi&nbsp;mai&nbsp;mica&nbsp;decat&nbsp;data&nbsp;inceput&nbsp;joc'</font><font color = "silver">;</font>
  <br/>
  <br/><font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>
  </font>

  <p>c) Sa se scrie comenzile SQL pentru popularea bazei de date cu urmatoarele doua jocuri: sah si table.</p>

  <font face="Courier New" size="2">
  <font color = "blue">INSERT</font>&nbsp;<font color = "blue">INTO</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font><font color = "maroon">tip_joc</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_inceput_joc</font><font color = "maroon">)</font>
  <br/><font color = "blue">VALUES</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font><font color = "red">'sah'</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "#FF0080"><b>Now</b></font><font color = "maroon">(</font><font color = "maroon">)</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font><font color = "red">'table'</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "#FF0080"><b>Now</b></font><font color = "maroon">(</font><font color = "maroon">)</font><font color = "maroon">)</font><font color = "silver">;</font>&nbsp;
  </font>

  <p>d) Sa se scrie o procedura stocata care creaza un nou joc, procedura primeste ca si parametru tipul jocului, numele celor doi jucatori si numarul de partide, numarul de partide trebuie sa fie impar.</p>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">PROCEDURE</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">creaza_joc</font><font color = "maroon">(</font><font color = "blue">IN</font>&nbsp;<font color = "maroon">tip_joc</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>VARCHAR</i></font><font color = "maroon">(</font><font color = "black">20</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IN</font>&nbsp;<font color = "maroon">nume_jucator1</font>&nbsp;<font color = "black"><i>VARCHAR</i></font><font color = "maroon">(</font><font color = "black">30</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IN</font>&nbsp;<font color = "maroon">nume_jucator2</font>&nbsp;<font color = "black"><i>VARCHAR</i></font><font color = "maroon">(</font><font color = "black">30</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IN</font>&nbsp;<font color = "maroon">nr_partide</font>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "black"><i>INT</i></font><font color = "maroon">(</font><font color = "black">3</font><font color = "maroon">)</font><font color = "maroon">)</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font><font color = "maroon">nr_partide</font>&nbsp;<font color = "silver">%</font>&nbsp;<font color = "black">2</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "black">0</font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">signal</font>&nbsp;<font color = "blue">SQLSTATE</font>&nbsp;<font color = "red">'90007'</font>&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">message_text</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "red">'numarul&nbsp;de&nbsp;partide&nbsp;trebuie&nbsp;sa&nbsp;fie&nbsp;impar'</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "blue">ELSE</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">INSERT</font>&nbsp;<font color = "blue">INTO</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">tip_joc</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">jucator1</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">jucator2</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">nr_partide</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">data_inceput_joc</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">value</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">tip_joc</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SELECT</font>&nbsp;<font color = "maroon">id_jucator</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">FROM</font>&nbsp;&nbsp;&nbsp;<font color = "maroon">jucatori</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">WHERE</font>&nbsp;&nbsp;<font color = "maroon">nume</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">nume_jucator1</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">LIMIT</font>&nbsp;&nbsp;<font color = "black">1</font><font color = "maroon">)</font><font color = "silver">,</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">(</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SELECT</font>&nbsp;<font color = "maroon">id_jucator</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">FROM</font>&nbsp;&nbsp;&nbsp;<font color = "maroon">jucatori</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">WHERE</font>&nbsp;&nbsp;<font color = "maroon">nume</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">nume_jucator2</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">LIMIT</font>&nbsp;&nbsp;<font color = "black">1</font><font color = "maroon">)</font><font color = "silver">,</font>&nbsp;<font color = "maroon">nr_partide</font><font color = "silver">,</font>&nbsp;<font color = "maroon">now</font><font color = "maroon">(</font><font color = "maroon">)</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "maroon">)</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;
  <br/>&nbsp;&nbsp;<font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>
  </font>

  <p>e) Sa se implementeze un trigger care sa seteze Invingatorul si Data de Sfarsit a unui joc daca Nr Partide a devenit egal cu NrPartide Jucate.</p>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t4_insert</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">INSERT</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font>&nbsp;<font color = "silver">&lt;&gt;</font>&nbsp;<font color = "black">0</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font>&nbsp;<font color = "silver">&lt;&gt;</font>&nbsp;<font color = "black">0</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font>&nbsp;<font color = "blue">IS</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">NULL</font>&nbsp;<font color = "maroon">)</font>&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">now</font><font color = "maroon">(</font><font color = "maroon">)</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">invingator</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">(</font><font color = "blue">SELECT</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IF</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">scor_jucator1</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">scor_jucator2</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">jucator1</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">jucator2</font><font color = "maroon">)</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>

  <br><br><br>

  <font face="Courier New" size="2">
  <font color = "blue">CREATE</font>&nbsp;<font color = "blue">TRIGGER</font>&nbsp;<font color = "maroon">jocuri_t4_update</font>&nbsp;<font color = "blue">BEFORE</font>&nbsp;<font color = "blue">UPDATE</font>
  <br/><font color = "blue">ON</font>&nbsp;<font color = "maroon">jocuri</font>
  <br/><font color = "blue">FOR</font>&nbsp;<font color = "blue">EACH</font>&nbsp;<font color = "maroon">row</font>
  <br/><font color = "maroon">begin</font>
  <br/>&nbsp;&nbsp;<font color = "blue">IF</font>&nbsp;<font color = "maroon">(</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font>&nbsp;<font color = "silver">&lt;&gt;</font>&nbsp;<font color = "black">0</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font>&nbsp;<font color = "silver">&lt;&gt;</font>&nbsp;<font color = "black">0</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">nr_partide_jucate</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">AND</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font>&nbsp;<font color = "blue">IS</font>&nbsp;<font color = "blue">NOT</font>&nbsp;<font color = "blue">NULL</font>&nbsp;<font color = "maroon">)</font>&nbsp;<font color = "blue">THEN</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">SET</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">data_sfarsit_joc</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">now</font><font color = "maroon">(</font><font color = "maroon">)</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">invingator</font>&nbsp;<font color = "silver">=</font>&nbsp;<font color = "maroon">(</font><font color = "blue">SELECT</font>
  <br/>&nbsp;&nbsp;&nbsp;&nbsp;<font color = "blue">IF</font><font color = "maroon">(</font><font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">scor_jucator1</font>&nbsp;<font color = "silver">&gt;</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">scor_jucator2</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">jucator1</font><font color = "silver">,</font>&nbsp;<font color = "maroon">new</font><font color = "silver">.</font><font color = "maroon">jucator2</font><font color = "maroon">)</font><font color = "maroon">)</font><font color = "silver">;</font>
  <br/>&nbsp;&nbsp;<font color = "maroon">end</font>&nbsp;<font color = "blue">IF</font><font color = "silver">;</font>
  <br/><font color = "maroon">end</font>&nbsp;
  </font>
</section>

<?php require_once 'extra/footer.php' ?>
