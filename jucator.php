<?php

$title = 'Jucator';
$page = 'jucatori';

require_once 'extra/connection.php';
require_once 'extra/head.php';
require_once 'extra/meniu.php';

if (!isset($_REQUEST['action'])) {
  header('Location: jucatori.php');
}

if (($_REQUEST['action'] == 'sterge') && isset($_REQUEST["id"])) {
  $conn->query('DELETE FROM jucatori WHERE id_jucator = ' . $_REQUEST["id"]);
  header('Location: jucatori.php');
} else if (($_REQUEST['action'] == 'sterge') && !isset($_REQUEST["id"])) {
  header('Location: jucatori.php');
}

if ($_REQUEST["action"] == 'adauga') {
  $jucator = false;
} else if ((($_REQUEST["action"] == 'modifica') || ($_REQUEST["action"] == 'detalii')) && isset($_REQUEST["id"])) {
  $result = $conn->query('SELECT * FROM jucatori WHERE id_jucator = ' . $_REQUEST["id"]);
  $jucator = $result->fetch();
} else {
  header('Location: jucatori.php');
}

if ($jucator) {
  //var_dump($jucator);
} else {
  //echo 'false';
}

?>

<section>
  <h1><?php echo $title; ?></h1>

  <p class="inapoi"><a href="jucatori.php" class="inpage">Lista ></a></p>

  <?php if (($_REQUEST["action"] == 'adauga') || (($_REQUEST["action"] == 'modifica') && $jucator)) { ?>
    <!-- FORMULARE BEGIN -->
    <form action="formular.php?from=jucator&action=<?php echo $_REQUEST["action"] ?>&id=<?php echo $_REQUEST["id"] ?>" method="POST">
      <label for="nume">Nume</label>
      <input type="text" id="nume" name="nume"<?php if ($jucator) echo ' value="' . $jucator["nume"] . '"'; ?> placeholder="Nume">

      <label for="data_nasterii">Data nasterii</label>
      <input type="date" id="data_nasterii" name="data_nasterii"<?php if ($jucator) echo ' value="' . $jucator["data_nasterii"] . '"'; ?> placeholder="Data nasterii">

      <label for="data_inscrierii">Data inscrierii</label>
      <input type="date" id="data_inscrierii" name="data_inscrierii"<?php if ($jucator) echo ' value="' . $jucator["data_inscrierii"] . '"'; ?> placeholder="Data inscrierii">

      <input type="submit" value="<?php echo (($_REQUEST["action"] == 'adauga') ? 'Adauga' : 'Salveaza') ?>">
    </form>
    <!-- FORMULARE END -->
  <?php } else if (($_REQUEST["action"] == 'detalii') && $jucator) { ?>
    <!-- DETALII BEGIN -->
    <table>
      <thead>
        <tr>
          <th width="200">Detaliu</th>
          <th>Valoare</th>
        </tr>
      </thead>

      <tbody>
        <?php foreach ($jucator as $key => $value) {
          if (!is_numeric($key)) {
            echo "<tr>";
            echo "<td>" . $key . "</td>";
            echo "<td>" . $value . "</td>";
            echo "</tr>";
          }
        } ?>
      </tbody>

      <tfoot>
        <tr>
          <td colspan="2" align="center">
            <a href="jucator.php?action=modifica&id=<?php echo $jucator["id_jucator"]; ?>" class="inpage">modificara</a> | <a href="jucator.php?action=sterge&id=<?php echo $jucator["id_jucator"]; ?>" onclick="return confirm('Sigur vrei sa stergi?')" class="inpage">sterge</a>
          </td>
        </tr>
      </tfoot>
    </table>

    <h2>Jocuri</h2>

    <table>
      <thead>
        <tr>
          <th>Nume</th>
          <th>Id joc</th>
          <th>Tip joc</th>
          <th>Data inceput joc</th>
          <th>Data sfarsit joc</th>
          <th>Castigator</th>
        </tr>
      </thead>

      <tbody>
        <?php

        $result = $conn->query("select nume, id_joc, tip_joc, data_inceput_joc, data_sfarsit_joc, if(invingator = id_jucator, 'da', 'nu') as catigator from jucatori inner join jocuri on id_jucator = jucator1 or id_jucator = jucator2 where id_jucator = " . $jucator["id_jucator"]);
        $jocuri = $result->fetchAll();

        foreach ($jocuri as $key => $value) {
          echo "<tr>";
          echo "<td align='center'>" . $value["nume"] . "</td>";
          echo "<td align='center'><a href='joc.php?action=detalii&id=" . $value["id_joc"] . "' class='inpage'>" . $value["id_joc"] . "</td>";
          echo "<td align='center'>" . $value["tip_joc"] . "</td>";
          echo "<td align='center'>" . $value["data_inceput_joc"] . "</td>";
          echo "<td align='center'>" . $value["data_sfarsit_joc"] . "</td>";
          echo "<td align='center'>" . $value["catigator"] . "</td>";
          echo "</tr>";
        } ?>
      </tbody>
    </table>
    <!-- DETALII END -->
  <?php } else { ?>
    <!-- EROARE BEGIN -->
    <h2 style="color: red">Nu s-a gasit nici un rezultat!</h2>
    <!-- EROARE END -->
  <?php } ?>
</section>

<?php require_once 'extra/footer.php' ?>
