<?php

$title = 'Joc';
$page = 'jocuri';

require_once 'extra/connection.php';
require_once 'extra/head.php';
require_once 'extra/meniu.php';

if (!isset($_REQUEST['action'])) {
  header('Location: jocuri.php');
}

if (($_REQUEST['action'] == 'sterge') && isset($_REQUEST["id"])) {
  $conn->query('DELETE FROM jocuri WHERE id_joc = ' . $_REQUEST["id"]);
  header('Location: jocuri.php');
} else if (($_REQUEST['action'] == 'sterge') && !isset($_REQUEST["id"])) {
  header('Location: jocuri.php');
}

if ($_REQUEST["action"] == 'adauga') {
  $jucator = false;
} else if ((($_REQUEST["action"] == 'modifica') || ($_REQUEST["action"] == 'detalii')) && isset($_REQUEST["id"])) {
  $result = $conn->query('SELECT id_joc, tip_joc, jucator1, (select nume from jucatori where id_jucator = jucator1) as nume_jucator1, jucator2, (select nume from jucatori where id_jucator = jucator2) as nume_jucator2, nr_partide, nr_partide_jucate, data_inceput_joc, data_sfarsit_joc, scor_jucator1, scor_jucator2, invingator, (select nume from jucatori where id_jucator = invingator) as nume_invingator FROM jocuri WHERE id_joc = ' . $_REQUEST["id"]);
  $joc = $result->fetch();
} else {
  header('Location: jocuri.php');
}

if ($joc) {
  //var_dump($joc);
} else {
  //echo 'false';
}

$not_to_show = array('jucator1', 'jucator2', 'invingator');

$tmp = $conn->query("SELECT * FROM jucatori");
$jucatori = $tmp->fetchAll();

?>

<section>
  <h1><?php echo $title; ?></h1>

  <p class="inapoi"><a href="jocuri.php" class="inpage">Lista ></a></p>

  <?php if (($_REQUEST["action"] == 'adauga') || (($_REQUEST["action"] == 'modifica') && $joc)) { ?>
    <!-- FORMULARE BEGIN -->
    <form action="formular.php?from=joc&action=<?php echo $_REQUEST["action"] ?>&id=<?php echo $_REQUEST["id"] ?>" method="POST">
      <label for="tip_joc">Tip joc</label>
      <input type="text" id="tip_joc" name="tip_joc"<?php if ($joc) echo ' value="' . $joc["tip_joc"] . '"'; ?> placeholder="Tip joc">

      <label for="jucator1">Jucator 1</label>
      <select id="jucator1" name="jucator1">
        <option value="">Selecteaza jucatorul</option>
        <?php foreach ($jucatori as $key => $value) {
          echo "<option value='" . $value["id_jucator"] . "'";

          if ($value["id_jucator"] == $joc["jucator1"]) {
            echo " selected";
          }

          echo ">" . $value["nume"] . "</option>";
        } ?>
      </select>

      <label for="jucator2">Jucator 2</label>
      <select id="jucator2" name="jucator2">
        <option value="">Selecteaza jucatorul</option>
        <?php foreach ($jucatori as $key => $value) {
          echo "<option value='" . $value["id_jucator"] . "'";

          if ($value["id_jucator"] == $joc["jucator2"]) {
            echo " selected";
          }

          echo ">" . $value["nume"] . "</option>";
        } ?>
      </select>

      <label for="nr_partide">Numar partide</label>
      <input type="number" id="nr_partide" name="nr_partide"<?php if ($joc) echo ' value="' . $joc["nr_partide"] . '"'; ?> placeholder="Numar partide">

      <label for="nr_partide_jucate">Numar pardite jucate</label>
      <input type="number" id="nr_partide_jucate" name="nr_partide_jucate"<?php if ($joc) echo ' value="' . $joc["nr_partide_jucate"] . '"'; ?> placeholder="Numar partide jucate">

      <label for="data_inceput_joc">Data inceput joc</label>
      <input type="datetime-local" id="data_inceput_joc" name="data_inceput_joc"<?php if ($joc) echo ' value="' . str_replace(' ', 'T', $joc["data_inceput_joc"]) . '"'; ?> placeholder="Data inceput joc">

      <label for="data_sfarsit_joc">Data sfarsit joc</label>
      <input type="datetime-local" id="data_sfarsit_joc" name="data_sfarsit_joc"<?php if ($joc) echo ' value="' . str_replace(' ', 'T', $joc["data_sfarsit_joc"]) . '"'; ?> placeholder="Data sfarsit joc">

      <label for="scor_jucator1">Scor jucator 1</label>
      <input type="number" id="scor_jucator1" name="scor_jucator1"<?php if ($joc) echo ' value="' . $joc["scor_jucator1"] . '"'; ?> placeholder="Scor jucator 1">

      <label for="scor_jucator2">Scor jucator 2</label>
      <input type="number" id="scor_jucator2" name="scor_jucator2"<?php if ($joc) echo ' value="' . $joc["scor_jucator2"] . '"'; ?> placeholder="Scor jucator 2">

      <input type="submit" value="<?php echo (($_REQUEST["action"] == 'adauga') ? 'Adauga' : 'Salveaza') ?>">
    </form>
    <!-- FORMULARE END -->
  <?php } else if (($_REQUEST["action"] == 'detalii') && $joc) { ?>
    <!-- DETALII BEGIN -->
    <table>
      <thead>
        <tr>
          <th width="200">Detaliu</th>
          <th>Valoare</th>
        </tr>
      </thead>

      <tbody>
        <?php foreach ($joc as $key => $value) {
          if (!is_numeric($key) && !in_array($key, $not_to_show)) {
            echo "<tr>";
            echo "<td>" . $key . "</td>";
            echo "<td>";

            if (($key == 'nume_jucator1') && !empty($value)) {
              echo "<a href='jucator.php?action=detalii&id=" . $joc["jucator1"] . "' class='inpage'>" . $value . "</a>";
            } else if (($key == 'nume_jucator2') && !empty($value)) {
              echo "<a href='jucator.php?action=detalii&id=" . $joc["jucator2"] . "' class='inpage'>" . $value . "</a>";
            } else if (($key == 'invingator') && !empty($value)) {
              echo "<a href='jucator.php?action=detalii&id=" . $joc["invingator"] . "' class='inpage'>" . $value . "</a>";
            } else {
              echo $value;
            }

            echo "</td>";
            echo "</tr>";
          }
        } ?>
      </tbody>

      <tfoot>
        <tr>
          <td colspan="2" align="center">
            <a href="joc.php?action=modifica&id=<?php echo $joc["id_joc"]; ?>" class="inpage">modificara</a> | <a href="joc.php?action=sterge&id=<?php echo $joc["id_joc"]; ?>" onclick="return confirm('Sigur vrei sa stergi?')" class="inpage">sterge</a>
          </td>
        </tr>
      </tfoot>
    </table>
    <!-- DETALII END -->
  <?php } else { ?>
    <!-- EROARE BEGIN -->
    <h2 style="color: red">Nu s-a gasit nici un rezultat!</h2>
    <!-- EROARE END -->
  <?php } ?>
</section>

<?php require_once 'extra/footer.php' ?>
