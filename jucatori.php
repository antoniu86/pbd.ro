<?php

$title = 'Jucatori';
$page = 'jucatori';

require_once 'extra/connection.php';
require_once 'extra/head.php';
require_once 'extra/meniu.php';

?>

<section>
  <h1><?php echo $title; ?></h1>

  <?php if ($_REQUEST["error"]) { ?>
    <p style="color:red"><?php echo urldecode($_REQUEST["error"]); ?></p>
  <?php } ?>

  <table class="first_last_center">
    <thead>
      <tr>
        <th width="50">Id</th>
        <th>Nume</th>
        <th>Data nasterii</th>
        <th>Data inscrierii</th>
        <th width="200">Optiuni</th>
      </tr>
    </thead>

    <tbdoy>
      <?php $tmp = $conn->query("SELECT * FROM jucatori"); ?>
      <?php $results = $tmp->fetchAll(); ?>

      <?php //var_dump($results); ?>

      <?php if(empty($results)) { ?>
        <tr><td colspan="100">Nu sunt rezultate</td></tr>
      <?php } else {
        foreach ($results as $key => $value) {
          echo "<tr>";
          echo "<td>" . $value["id_jucator"] . "</td><td>" . $value["nume"] . "</td><td>" . $value["data_nasterii"] . "</td><td>" . $value["data_inscrierii"] . "</td>";
          echo "<td><a href='jucator.php?action=detalii&id=" . $value["id_jucator"] . "' class=\"inpage\">detalii</a> | <a href='jucator.php?action=modifica&id=" . $value["id_jucator"] . "' class=\"inpage\">modificara</a> | <a href='jucator.php?action=sterge&id=" . $value["id_jucator"] . "' onclick=\"return confirm('Sigur vrei sa stergi?')\" class=\"inpage\">sterge</a></td>";
          echo "</tr>";
        }
      } ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="100" align="center">
          <a href="jucator.php?action=adauga" class="inpage">Adauga</a>
        </td>
      </tr>
    </tfoot>
  </table>
</section>

<?php require_once 'extra/footer.php' ?>
