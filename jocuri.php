<?php

$title = 'Jocuri';
$page = 'jocuri';

require_once 'extra/connection.php';
require_once 'extra/head.php';
require_once 'extra/meniu.php';

?>

<section>
  <h1><?php echo $title; ?></h1>

  <?php if ($_REQUEST["error"]) { ?>
    <p style="color:red"><?php echo urldecode($_REQUEST["error"]); ?></p>
  <?php } ?>

  <table class="first_last_center">
    <thead>
      <tr>
        <th width="50">Id</th>
        <th>Tip</th>
        <th>Jucatori 1</th>
        <th>Jucator 2</th>
        <th>Partide</th>
        <th>Partide jucate</th>
        <th>Inceput</th>
        <th>Sfarsit</th>
        <th>Scor jucator 1</th>
        <th>Scor jucator 2</th>
        <th>Invingator</th>
        <th width="100">Optiuni</th>
      </tr>
    </thead>

    <tbdoy>
      <?php

      $tmp = $conn->query("SELECT id_joc, tip_joc, jucator1, (select nume from jucatori where id_jucator = jucator1) as nume_jucator1, jucator2, (select nume from jucatori where id_jucator = jucator2) as nume_jucator2, nr_partide, nr_partide_jucate, data_inceput_joc, data_sfarsit_joc, scor_jucator1, scor_jucator2, invingator, (select nume from jucatori where id_jucator = invingator) as nume_invingator FROM jocuri");

      $results = $tmp->fetchAll();

      //var_dump($results);

      ?>

      <?php if(empty($results)) { ?>
        <tr><td colspan="100">Nu sunt rezultate</td></tr>
      <?php } else {
        foreach ($results as $key => $value) {
          echo "<tr>";
          echo "<td>" . $value["id_joc"] . "</td><td>" . $value["tip_joc"] . "</td><td>" . $value["nume_jucator1"] . "</td>";
          echo "<td>" . $value["nume_jucator2"] . "</td><td>" . $value["nr_partide"] . "</td><td>" . $value["nr_partide_jucate"] . "</td>";
          echo "<td>" . $value["data_inceput_joc"] . "</td><td>" . $value["data_sfarsit_joc"] . "</td>";
          echo "<td>" . $value["scor_jucator1"] . "</td><td>" . $value["scor_jucator2"] . "</td><td>" . $value["nume_invingator"] . "</td>";
          echo "<td><a href='joc.php?action=detalii&id=" . $value["id_joc"] . "'>detalii</a> | <a href='joc.php?action=modifica&id=" . $value["id_joc"] . "'  class=\"inpage\">modificara</a> | <a href='joc.php?action=sterge&id=" . $value["id_joc"] . "' onclick=\"return confirm('Sigur vrei sa stergi?')\" class=\"inpage\">sterge</a></td>";
          echo "</tr>";
        }
      } ?>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="100" align="center">
          <a href="joc.php?action=adauga" class="inpage">Adauga</a>
        </td>
      </tr>
    </tfoot>
  </table>

  <h2>Raport jocuri finalizate intre 01.ian.2010 si 01.apr.2010</h2>

  <table class="first_center">
    <thead>
      <tr>
        <th width="50">Id joc</th>
        <th>Tip joc</th>
        <th>Nr. partide</th>
        <th>Data inceput joc</th>
        <th>Data sfarsit joc</th>
        <th>Durata (ore)</th>
        <th>Invingator</th>
      </tr>
    </thead>

    <tbdoy>
      <?php

      $tmp = $conn->query("select id_joc, tip_joc, nr_partide, data_inceput_joc, data_sfarsit_joc, TIMESTAMPDIFF(HOUR, data_inceput_joc, data_sfarsit_joc) as diferenta, invingator, (select nume from jucatori where id_jucator = invingator) as nume_invingator from jocuri where data_sfarsit_joc between '2010-01-01' and '2010-04-01' order by tip_joc asc, data_inceput_joc asc");

      //var_dump($tmp);

      $results = $tmp->fetchAll();

      //var_dump($results);

      ?>

      <?php if(empty($results)) { ?>
        <tr><td colspan="100">Nu sunt rezultate</td></tr>
      <?php } else {
        foreach ($results as $key => $value) {
          echo "<tr>";
          echo "<td>" . $value["id_joc"] . "</td>";
          echo "<td>" . $value["tip_joc"] . "</td>";
          echo "<td>" . $value["nr_partide"] . "</td>";
          echo "<td>" . $value["data_inceput_joc"] . "</td>";
          echo "<td>" . $value["data_sfarsit_joc"] . "</td>";
          echo "<td>" . $value["diferenta"] . "</td>";

          if (!is_null($value["invingator"])) {
            echo "<td><a href='jucator.php?action=detalii&id=" . $value["invingator"] . "' class='inpage'>" . $value["nume_invingator"] . "</td>";
          } else {
            echo "<td></td>";
          }

          echo "</tr>";
        }
      } ?>
    </tbody>
  </table>
</section>

<?php require_once 'extra/footer.php' ?>
