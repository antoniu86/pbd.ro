<?php

error_reporting(E_ALL);

if (isset($_REQUEST["from"], $_REQUEST["action"])) {
  require_once('extra/connection.php');

  if ($_REQUEST["from"] == 'jucator') {
    if ($_REQUEST["action"] == 'adauga') {
      $sql = "INSERT INTO jucatori
      (nume, data_nasterii, data_inscrierii)
      VALUE (
        " . (empty($_POST['nume']) ? 'NULL' : "'" . $_POST['nume'] . "'") . ",
        " . (empty($_POST['data_nasterii']) ? 'NULL' : "'" . $_POST['data_nasterii'] . "'") . ",
        " . (empty($_POST['data_inscrierii']) ? 'NULL' : "'" . $_POST['data_inscrierii'] . "'") . "
      )";

      if ($conn->query($sql)) {
        header('Location: jucatori.php');
      } else {
        $db_error = $conn->errorInfo();
        $error = $db_error[0] . " - " . $db_error[2];
        header('Location: jucatori.php?error=' . urlencode($error));
      }
    }

    if ($_REQUEST["action"] == 'modifica') {
      $modificari = array();

      foreach ($_POST as $key => $value) {
        if (!empty($value)) {
          array_push($modificari, $key . " = '$value'");
        }
      }

      $sql = "UPDATE jucatori SET " . implode(' , ', $modificari) . " WHERE id_jucator = " . $_REQUEST["id"];

      if ($conn->query($sql)) {
        header('Location: jucatori.php');
      } else {
        $db_error = $conn->errorInfo();
        $error = $db_error[0] . " - " . $db_error[2];
        header('Location: jucatori.php?error=' . urlencode($error));
      }
    }
  } else if ($_REQUEST["from"] == 'joc') {
    if ($_REQUEST["action"] == 'adauga') {
      $sql = "INSERT INTO jocuri
      (tip_joc, jucator1, jucator2, nr_partide, nr_partide_jucate, data_inceput_joc, scor_jucator1, scor_jucator2)
      VALUE (
        " . (empty($_POST['tip_joc']) ? 'NULL' : "'" . $_POST['tip_joc'] . "'") . ",
        " . (empty($_POST['jucator1']) ? 'NULL' : "'" . $_POST['jucator1'] . "'") . ",
        " . (empty($_POST['jucator2']) ? 'NULL' : "'" . $_POST['jucator2'] . "'") . ",
        " . (empty($_POST['nr_partide']) ? 'NULL' : "'" . $_POST['nr_partide'] . "'") . ",
        " . (empty($_POST['nr_partide_jucate']) ? 'NULL' : "'" . $_POST['nr_partide_jucate'] . "'") . ",
        " . (empty($_POST['data_inceput_joc']) ? 'NULL' : "'" . $_POST['data_inceput_joc'] . "'") . ",
        " . (empty($_POST['data_sfarsit_joc']) ? 'NULL' : "'" . $_POST['data_sfarsit_joc'] . "'") . ",
        " . (empty($_POST['scor_jucator1']) ? 'NULL' : "'" . $_POST['scor_jucator1'] . "'") . ",
        " . (empty($_POST['scor_jucator2']) ? 'NULL' : "'" . $_POST['scor_jucator2'] . "'") . "
      )";

      if ($conn->query($sql)) {
        header('Location: jocuri.php');
      } else {
        $db_error = $conn->errorInfo();
        $error = $db_error[0] . " - " . $db_error[2];
        header('Location: jocuri.php?error=' . urlencode($error));
      }
    }

    if ($_REQUEST["action"] == 'modifica') {
      $modificari = array();

      foreach ($_POST as $key => $value) {
        if (!empty($value)) {
          array_push($modificari, $key . " = '$value'");
        }
      }

      $sql = "UPDATE jocuri SET " . implode(' , ', $modificari) . " WHERE id_joc = " . $_REQUEST["id"];

      if ($conn->query($sql)) {
        header('Location: jocuri.php');
      } else {
        $db_error = $conn->errorInfo();
        $error = $db_error[0] . " - " . $db_error[2];
        header('Location: jocuri.php?error=' . urlencode($error));
      }
    }
  }

  //
} else {
  header('Location: index.php');
}

?>
